<?php
/**
 * @file
 * quiz_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function quiz_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'quiz';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Quiz';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Test';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'mer';
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Bruk';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Nullstill';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sortér på';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Stigende';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Synkende';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Elementer per side';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Alle -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Forskyving';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« første';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ forrige';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'neste ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'siste »';
  $handler->display->display_options['style_plugin'] = 'footable';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'created' => 'created',
    'realname' => 'realname',
    'parent_nid' => 'parent_nid',
    'pass_rate' => 'pass_rate',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'realname' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'parent_nid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'pass_rate' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['footable'] = array(
    'expand' => 'title',
    'icon' => '',
    'icon_size' => '',
    'hide' => array(
      'title' => array(
        'phone' => 0,
        'tablet' => 0,
      ),
      'created' => array(
        'phone' => 'phone',
        'tablet' => 0,
      ),
      'realname' => array(
        'phone' => 'phone',
        'tablet' => 0,
      ),
      'parent_nid' => array(
        'phone' => 'phone',
        'tablet' => 0,
      ),
      'pass_rate' => array(
        'phone' => 'phone',
        'tablet' => 0,
      ),
    ),
  );
  /* Relationship: Quiz question relationship: Parent_nid */
  $handler->display->display_options['relationships']['parent_nid']['id'] = 'parent_nid';
  $handler->display->display_options['relationships']['parent_nid']['table'] = 'quiz_node_relationship';
  $handler->display->display_options['relationships']['parent_nid']['field'] = 'parent_nid';
  /* Relationship: Innhold: Forfatter */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Innhold: Tittel */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Innhold: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Opprettet';
  $handler->display->display_options['fields']['created']['date_format'] = 'medium';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Realname: Real name */
  $handler->display->display_options['fields']['realname']['id'] = 'realname';
  $handler->display->display_options['fields']['realname']['table'] = 'realname';
  $handler->display->display_options['fields']['realname']['field'] = 'realname';
  $handler->display->display_options['fields']['realname']['relationship'] = 'uid';
  $handler->display->display_options['fields']['realname']['label'] = 'Opprettet av';
  /* Field: COUNT(Quiz question relationship: Parent_nid) */
  $handler->display->display_options['fields']['parent_nid']['id'] = 'parent_nid';
  $handler->display->display_options['fields']['parent_nid']['table'] = 'quiz_node_relationship';
  $handler->display->display_options['fields']['parent_nid']['field'] = 'parent_nid';
  $handler->display->display_options['fields']['parent_nid']['group_type'] = 'count';
  $handler->display->display_options['fields']['parent_nid']['label'] = 'Antall spørsmål';
  /* Field: Quiz properties: Pass_rate */
  $handler->display->display_options['fields']['pass_rate']['id'] = 'pass_rate';
  $handler->display->display_options['fields']['pass_rate']['table'] = 'quiz_node_properties';
  $handler->display->display_options['fields']['pass_rate']['field'] = 'pass_rate';
  $handler->display->display_options['fields']['pass_rate']['label'] = 'Krav';
  $handler->display->display_options['fields']['pass_rate']['suffix'] = '%';
  /* Sort criterion: Innhold: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Innhold: Publisert */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Innhold: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'quiz' => 'quiz',
  );

  /* Display: Oversikt over quizer */
  $handler = $view->new_display('page', 'Oversikt over quizer', 'page');
  $handler->display->display_options['path'] = 'quiz';
  $translatables['quiz'] = array(
    t('Master'),
    t('Test'),
    t('mer'),
    t('Bruk'),
    t('Nullstill'),
    t('Sortér på'),
    t('Stigende'),
    t('Synkende'),
    t('Elementer per side'),
    t('- Alle -'),
    t('Forskyving'),
    t('« første'),
    t('‹ forrige'),
    t('neste ›'),
    t('siste »'),
    t('Node'),
    t('forfatter'),
    t('Tittel'),
    t('Opprettet'),
    t('Opprettet av'),
    t('Antall spørsmål'),
    t('.'),
    t(','),
    t('Krav'),
    t('%'),
    t('Oversikt over quizer'),
  );
  $export['quiz'] = $view;

  return $export;
}
